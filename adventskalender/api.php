<?php
    $action = $_GET["action"];

    define("DAY", intval(date("d")));
    define("MONTH", intval(date("m")));
    define("ENABLED_MONTH", 12);
    define("ENABLED_UNTIL_DAY", 24);
    define("TEST_MODE", false);

    if (file_get_contents("storage/content.json") === "") {
        $content = [];

        for ($i = 0; $i < ENABLED_UNTIL_DAY; $i++) $content[$i] = "";

        file_put_contents("storage/content.json", json_encode($content));
    }

    switch($action) {
        case "GET_DOOR_CONTENT":
            $day = intval($_GET["day"]);

            if (($day > DAY || DAY > ENABLED_UNTIL_DAY || MONTH != ENABLED_MONTH) && !TEST_MODE) {
                echo json_encode([ "error" => "Bitte warten Sie auf das passende Datum." ]);
                return;
            }

            $content = json_decode(file_get_contents("storage/content.json"), true);
            echo json_encode([ "content" => $content[--$day] ]);
            break;
        case "GET_TEST_MODE_STATUS":
            echo json_encode([ "testMode" => TEST_MODE ]);
            break;
    }
?>