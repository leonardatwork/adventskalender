"use strict";

var days = [];
var enabledMonth = 12;
var enabledUntilDay = 24;
var allowedToOpen = true;

function closeCalendar() {
    document.body.style.overflow = "unset";

    document.querySelector("[data-calendar]").remove();
    Array.from(document.querySelectorAll(".snowflake")).map(function(snowflake) {
        return snowflake.remove();
    });
}

function closeDoor(door) {
    door.innerHTML = door.dataset.day;
    door.classList.remove("opened");

    setTimeout(function() {
        allowedToOpen = true;
    }, 150);
}

function openDoor(door) {
    if (!allowedToOpen || parseInt(door.innerText) > new Date().getDate()) return;
    allowedToOpen = false;

    var doorDay = door.innerText;
    
    door.classList.add("opened");
    door.innerHTML = "Das Türchen wird geöffnet...<a data-close onclick=\"closeDoor(this.parentElement)\">X</a>";

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200 && !this.responseText.includes("error")) {
                door.innerHTML = JSON.parse(this.responseText).content + "<a data-close onclick=\"closeDoor(this.parentElement)\">X</a>";
            }else {
                door.innerText = doorDay;
                door.classList.remove("opened");
            }
        }
    };
    xhttp.open("GET", "adventskalender/api.php?action=GET_DOOR_CONTENT&day=" + door.dataset.day, true);
    xhttp.send();
}

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
        if ((new Date().getDate() <= enabledUntilDay && new Date().getMonth() + 1 === enabledMonth) || this.responseText.includes("true")) {
            document.body.style.overflow = "hidden";
        
            var calendar = document.createElement("div");
            calendar.setAttribute("data-calendar", "");
            calendar.innerHTML = `<div>
                <a data-close onclick="closeCalendar()">X</a>
                <div class="branding">
                    <img src="adventskalender/exports/img/logo.png">
                </div>
            </div>`;
            document.body.appendChild(calendar);
        
            for (var i = 1; i < 25; i++) {
                days.push(i);
            }
        
            for (var j = 0; j < 50; j++) {
                var snowflake = document.createElement("div");
                snowflake.classList.add("snowflake");
                document.body.appendChild(snowflake);
            }
        
            days.sort(function () {
                return .5 - Math.random();
            }).map(function (day) {
                return document.querySelector("[data-calendar] > div").innerHTML += "<div onclick='openDoor(this)' ".concat(day <= new Date().getDate() ? "data-available" : "", " data-day=\"").concat(day, "\" data-door>").concat(day, "</div>");
            });
        }
    }
};
xhttp.open("GET", "adventskalender/api.php?action=GET_TEST_MODE_STATUS", true);
xhttp.send();