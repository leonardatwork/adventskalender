# Der Adventskalender
## Adventskalender einbinden
1. Ordner ``adventskalender`` in den Webspace laden
2. Dateiberechtigung der Datei ``/adventskalender/storage/content.json`` auf 777 setzen
3. ``/adventskalender/exports/css/adventskalender.css`` anpassen
4. PHP aktivieren lassen
5. Adventskalender in die Seite einbinden:
````html 
<link rel="stylesheet" href="adventskalender/exports/css/adventskalender.css">
<script src="adventskalender/exports/js/adventskalender.js"></script>
````

## Türcheninhalte anpassen
Datei ``/adventskalender/storage/content.json`` anpassen (HTML).
Ist ein einfaches Array (Index 0 ist Türchen 1, Index 1 ist Türchen 2, ...).

## Logo ändern
Datei im Verzeichnis ``/adventskalender/exports/img/logo.png`` ersetzen.

## Slogan ändern
Zeile 27 in der Datei ``/adventskalender/js/adventskalender.js`` anpassen.

## Testmodus aktivieren
Test-Mode (Zeile 8) in der Datei ``/adventskalender/api.php`` auf ``true`` setzen.

## Testmodus deaktivieren
Test-Mode (Zeile 8) in der Datei ``/adventskalender/api.php`` auf ``false`` setzen.